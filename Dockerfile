FROM registry.gitlab.com/jinhu/ruby:master
MAINTAINER Jin hu <Jin@hukelou.com>

RUN echo 'gem: --no-document' > ~/.gemrc
COPY Gemfile /app/
WORKDIR /app

RUN apk --update add --virtual build-dependencies build-base \
  && bundle install && gem clean \
  && apk del build-dependencies \
  && rm -vrf /var/cache/apk/*

CMD [ "bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "4567" ]
